# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake systemd-service

SUMMARY="Message broker that implements the MQTT protocol versions 5.0, 3.1.1 and 3.1"
DESCRIPTION="
Eclipse Mosquitto is an open source (EPL/EDL licensed) message broker that implements the MQTT
protocol versions 5.0, 3.1.1 and 3.1. Mosquitto is lightweight and is suitable for use on all
devices from low power single board computers to full servers.

The MQTT protocol provides a lightweight method of carrying out messaging using a
publish/subscribe model. This makes it suitable for Internet of Things messaging such as with low
power sensors or mobile devices such as phones, embedded computers or microcontrollers.

The Mosquitto project also provides a C library for implementing MQTT clients, and the very popular
mosquitto_pub and mosquitto_sub command line MQTT clients.
"
HOMEPAGE="https://mosquitto.org"
DOWNLOADS="${HOMEPAGE}/files/source/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/category/releases [[ lang = en ]]"

LICENCES="EDL-1.0 EPL-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    systemd
    tcpd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/uthash
    build+run:
        group/${PN}
        user/${PN}
        dev-libs/cjson
        net-dns/c-ares
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_SBINDIR:PATH=bin
    -DCMAKE_INSTALL_SYSCONFDIR:PATH=/etc
    -DDOCUMENTATION:BOOL=TRUE
    -DINC_BRIDGE_SUPPORT:BOOL=TRUE
    -DINC_MEMTRACK:BOOL=TRUE
    -DWITH_APPS:BOOL=TRUE
    -DWITH_BROKER:BOOL=TRUE
    -DWITH_BUNDLED_DEPS:BOOL=FALSE
    -DWITH_CJSON:BOOL=TRUE
    -DWITH_CLIENTS:BOOL=TRUE
    -DWITH_DLT:BOOL=FALSE
    -DWITH_EC:BOOL=TRUE
    -DWITH_PERSISTENCE:BOOL=TRUE
    -DWITH_PLUGINS:BOOL=TRUE
    -DWITH_SOCKS:BOOL=TRUE
    -DWITH_SRV:BOOL=TRUE
    -DWITH_STATIC_LIBRARIES:BOOL=FALSE
    -DWITH_SYS_TREE:BOOL=TRUE
    -DWITH_THREADING:BOOL=TRUE
    -DWITH_TLS:BOOL=TRUE
    -DWITH_TLS_PSK:BOOL=TRUE
    -DWITH_UNIX_SOCKETS:BOOL=TRUE
    -DWITH_WEBSOCKETS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'tcpd USE_LIBWRAP'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'systemd SYSTEMD'
)

src_prepare() {
    cmake_src_prepare

    edo sed \
        -e "s:^#autosave_interval:autosave_interval:" \
        -e "s:^#persistence false$:persistence true:" \
        -e "s:^#persistence_file:persistence_file:" \
        -e "s:^#persistence_location$:persistence_location /var/lib/mosquitto:" \
        -i mosquitto.conf
}

src_install() {
    cmake_src_install

    keepdir /var/lib/${PN}
    edo chown -R mosquitto:mosquitto "${IMAGE}"/var/lib/${PN}

    insinto ${SYSTEMDSYSTEMUNITDIR}
    newins "${CMAKE_SOURCE}"/service/systemd/${PN}.service.notify ${PN}.service
}

