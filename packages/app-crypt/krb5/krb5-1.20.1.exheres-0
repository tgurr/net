# Copyright 2013 Dirk Heinrichs
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="MIT implementation of the Kerberos V protocol"
DESCRIPTION="
This is the original implementation of the Kerberos V protocol for secure authentication.
"
HOMEPAGE="https://web.mit.edu/kerberos"
DOWNLOADS="https://kerberos.org/dist/${PN}/$(ever range 1-2)/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ldap
    libedit
    lmdb [[ description = [ Support LMDB as an alternative to the bundled DB2 ] ]]
"

# The tests want to run python, phone home and do all kinds of "funny" things.
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/libverto
        dev-libs/openssl:=[>=1.1]
        sys-apps/keyutils[>=1.6]
        sys-fs/e2fsprogs
        ldap? (
            net-directory/openldap
            net-libs/cyrus-sasl
        )
        libedit? ( dev-libs/libedit )
        lmdb? ( dev-db/lmdb )
        !app-crypt/heimdal [[
            description = [ (MIT) krb5 and Heimdal collide. Choose one. ]
            url = [ http://dev.exherbo.org/~philantrop/heimdal-krb5-collisions.txt ]
            resolution = manual
        ]]
    suggestion:
        sys-auth/pam-krb5
"

# --with-system-db is declared unsupported and untestet according to MIT docs.
DEFAULT_SRC_CONFIGURE_PARAMS=(
    # required as long as upstream uses autoconf < 2.70
    runstatedir=/run
    --enable-dns-for-realm
    --enable-nls
    --disable-asan
    --disable-rpath
    --with-crypto-impl=openssl
    --with-keyutils
    --with-readline
    --with-spake-openssl
    --with-system-et
    --with-system-ss
    --with-system-verto
    --with-tls-impl=openssl

    # NOTE: Assumes support for __attribute__((constructor)) in
    # order to fix cross-compiling. GCC and Clang support this.
    krb5_cv_attr_constructor_destructor=yes,yes
    # Both musl and glibc support regcomp and positional printf
    # specifiers, but krb5 can't check them when cross compiling
    ac_cv_func_regcomp=yes
    ac_cv_printf_positional=yes
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    ldap
    libedit
    lmdb
)

WORK=${WORKBASE}/${PNV}/src

src_install() {
    default

    # Remove empty directories
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/krb5/plugins/{authdata,libkrb5}
    edo rmdir "${IMAGE}"/usr/share/man/cat?
    edo rm -r "${IMAGE}"/run

    keepdir /var/lib/krb5kdc

    # Move the examples to DOCDIR
    dodir /usr/share/doc/${PNVR}
    edo cp -r "${IMAGE}"/usr/share/examples/krb5 "${IMAGE}"/usr/share/doc/${PNVR}
    edo rm -r "${IMAGE}"/usr/share/examples

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}-krb5kdc.conf <<EOF
d /run/krb5kdc 0755 root root
EOF

    hereconfd krb5kdc.conf <<EOF
# See krb5kdc(8) man page
KDC_ARGS=""
EOF

    if option ldap ; then
        insinto /etc/openldap/schema
        doins plugins/kdb/ldap/libkdb_ldap/kerberos.*
    fi
}

