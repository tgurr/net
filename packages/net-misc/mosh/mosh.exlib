# Copyright 2012 Julien Pivotto <roidelapluie@gmail.com>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_install

if ever is_scm;then
    require github [ user=keithw ]
    require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
else
    DOWNLOADS="http://mosh.mit.edu/${PNV}.tar.gz"
fi

SUMMARY="Mosh is a robust replacement for SSH".
DESCRIPTION="
Remote terminal application that allows roaming, supports
intermittent connectivity, and provides intelligent local echo and line
editing of user keystrokes. Mosh is a replacement for SSH. It's more robust
and responsive, especially over Wi-Fi, cellular, and long-distance links.
"
HOMEPAGE="http://mosh.mit.edu/"
MYOPTIONS="
    ( providers: libressl nettle openssl )
"

DEPENDENCIES="
    build+run:
        dev-lang/perl:*
        dev-libs/protobuf:=
        dev-perl/IO-Tty
        net-misc/openssh
        sys-libs/ncurses
        providers:libressl? ( dev-libs/libressl:= )
        providers:nettle? ( dev-libs/nettle:= )
        providers:openssl? ( dev-libs/openssl )
"

LICENCES="GPL-3"
SLOT="0"

UPSTREAM_CHANGELOG="https://github.com/keithw/mosh/blob/master/ChangeLog"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=enable-fast-install
    --disable-completion
    --disable-ufw
    --without-utempter
    --enable-client
    --enable-server
    --disable-examples
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    'providers:libressl --with-crypto-library=openssl'
    'providers:nettle --with-crypto-library=nettle'
    'providers:openssl --with-crypto-library=openssl'
)

# Tests use network, skip them
RESTRICT="test"

mosh_src_install() {
    default

    [[ -d "${IMAGE}"/etc ]] && edo rmdir "${IMAGE}"/etc
}

