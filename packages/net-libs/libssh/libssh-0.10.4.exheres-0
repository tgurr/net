# Copyright 2009-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014-2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="The ssh library was designed to be used by programmers needing a working SSH implementation by the means of a library"
HOMEPAGE="https://www.${PN}.org/"
DOWNLOADS="${HOMEPAGE}files/$(ever range -2)/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}2018/12/24/${PNV/./-}-xmas-edition [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}documentation/ [[ lang = en ]]"

LICENCES="LGPL-2.1 BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    examples
    kerberos
    nacl [[ description = [ Support key exchange methode curve25519-sha256@libssh.org ] ]]
    pcap [[ description = [ Capture decrypted network traffic with libpcap for debugging ] ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: gcrypt libressl mbedtls openssl ) [[ number-selected = exactly-one ]]
    ( libc: musl )
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen[dot] )
    build+run:
        sys-libs/zlib[>=1.2]
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        nacl? ( dev-libs/nacl )
        pcap? ( dev-libs/libpcap )
        providers:gcrypt? ( dev-libs/libgcrypt[>=1.5] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:mbedtls? ( dev-libs/mbedtls )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.1] )
    test:
        dev-util/cmocka
        net-misc/openssh
        libc:musl? ( dev-libs/argp-standalone )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DFUZZ_TESTING:BOOL=FALSE
    # Would need openssh, socket_wrapper, nss_wrapper, uid_wrapper, pam_wrapper
    -DCLIENT_TESTING:BOOL=FALSE
    # Would need openssh and dropbear
    -DSERVER_TESTING:BOOL=FALSE
    -DWITH_INSECURE_NONE:BOOL=FALSE
    # Would need softhsm and is only supported with openssl[>=1.1.1]
    -DWITH_PKCS11_URI:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'doc INTERNAL_DOC'
    EXAMPLES
    'providers:gcrypt GCRYPT'
    'providers:mbedtls MBEDTLS'
    'kerberos GSSAPI'
    NACL
    PCAP
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'doc Doxygen'
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DUNIT_TESTING:BOOL=TRUE -DUNIT_TESTING:BOOL=FALSE'
)

src_prepare() {
    cmake_src_prepare

    # Test works under a normal user, but fails because of paludisbuild's enviroment
    edo sed -e '/unit_test(torture_path_expand_tilde_unix),/d' \
            -i tests/unittests/torture_misc.c

    if [[ $(exhost --target) == *-musl* ]]; then
        # On musl we need to link against argp-standalone, remove the `if` block around
        # `find_package(Argp)`
        edo sed -e '/if.*SOLARIS/d' -i tests/CMakeLists.txt
    fi
}

src_compile() {
    default

    option doc && emake docs
}

src_test() {
    # If this isn't set torture_config is confused about the expansion of ~
    export HOME="/var/tmp/paludis"

    default
}
src_install() {
    cmake_src_install

    if option doc ; then
        docinto html
        dodoc -r doc/html/*
    fi

    if option examples ; then
        docinto examples
        dodoc -r "${CMAKE_SOURCE}"/examples/*
    fi
}

