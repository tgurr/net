# Copyright 2022-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi [ pnv=${PNV/-/_} ] setup-py [ import=setuptools blacklist=2 has_bin=true test=pytest work=${PNV/-/_} ] \
    systemd-service

SUMMARY="A vulnerability scanner for creating results from local security checks (LSCs)"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/gvm
        user/gvm
        dev-python/packaging[>=20.5][python_abis:*(-)?]
        dev-python/paho-mqtt[>=1.5.1][python_abis:*(-)?]
        dev-python/psutil[>=5.9&<6.0.0][python_abis:*(-)?]
        dev-python/python-gnupg[>=0.4.6&<0.5.0][python_abis:*(-)?]
        dev-python/tomli[>=1.0.0][python_abis:*(-)?]
    run:
        net/mosquitto
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/9c5588e43b25609db13033d6a373bb97d52a7ea0.patch
)

install_one_multibuild() {
    setup-py_install_one_multibuild

    install_systemd_files

    insinto /etc/gvm
    doins "${FILES}"/notus-scanner.toml

    keepdir /var/log/gvm
    edo chown gvm:gvm "${IMAGE}"/var/log/gvm

    # do not install test stuff into site-packages
    edo rm -rf "${IMAGE}"$(python_get_libdir)/site-packages/{setup.py,__pycache__,tests}
}

