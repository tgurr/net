# Copyright 2013-2019 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Tomcat Connectors (mod_jk) for Apache httpd"
HOMEPAGE="https://tomcat.apache.org/connectors-doc/"
DOWNLOADS="mirror://apache/tomcat/${PN}/jk/${PNV}-src.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        www-servers/apache[>=2.2]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --with-apxs
)

WORK+=-src/native

src_prepare() {
    # Override apxs' installation path to avoid a sandbox violation
    edo sed -i -e "s:\(-i\) \(mod_jk.la\):\1 -S LIBEXECDIR=${IMAGE}/usr/$(exhost --target)/libexec/apache2/modules \2:" apache-2.0/Makefile.in

    autotools_src_prepare
}

src_install() {
    # Since we override the apxs default, we need to create the dir manually
    dodir /usr/$(exhost --target)/libexec/apache2/modules

    default

    # install sample configurations & html docs
    edo pushd ..
    dodoc -r conf
    dodoc -r docs
    edo mv "${IMAGE}"/usr/share/doc/${PNVR}/{docs,html}

    # install minimal configuration
    insinto /etc/apache2/modules.d
    newins conf/httpd-jk.conf 80_mod_jk.conf
    insinto /etc/apache2/conf
    doins conf/workers.properties

    edo popd
}

