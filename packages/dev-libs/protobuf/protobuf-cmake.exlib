# Copyright 2011-2018 Timo Gurr <tgurr@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=google release=v${PV} pnv=${PN}-cpp-${PV} suffix=tar.gz ]
require cmake alternatives

export_exlib_phases src_configure src_install

SUMMARY="Protocol Buffers - Google's data interchange format"
DESCRIPTION="
Protocol buffers are a flexible, efficient, automated mechanism for serializing
structured data - think XML, but smaller, faster, and simpler. You define how
you want your data to be structured once, then you can use special generated
source code to easily write and read your structured data to and from a variety
of data streams and using a variety of languages. You can even update your data
structure without breaking deployed programs that are compiled against the old
format.
"

LICENCES="BSD-3 vim-syntax? ( vim )"
SLOT="$(ever range 1-2)"
MYOPTIONS="vim-syntax"

# TODO: options: emacs examples java
DEPENDENCIES="
    build+run:
        sys-libs/zlib[>=1.2.0.4]
    run:
        !dev-libs/protobuf:0[<3.1.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !dev-libs/protobuf:0[>3.2&<3.3.0-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    post:
        vim-syntax? ( app-editors/vim-runtime:*[>=7] )
"

CMAKE_SOURCE=${WORKBASE}/${PNV}/cmake

protobuf-cmake_src_configure() {
    local myconf=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -Dprotobuf_WITH_ZLIB:BOOL=TRUE
    )
    if ! exhost --is-native -q; then
        myconf+=( -Dprotobuf_BUILD_PROTOC_BINARIES:BOOL=TRUE )
    fi

    ecmake "${myconf[@]}"
}

protobuf-cmake_src_install() {
    local alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    alternatives+=(
        /usr/${host}/bin/protoc                  protoc-${SLOT}
        /usr/${host}/include/google/${PN}        ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so             lib${PN}-${SLOT}.so
        /usr/${host}/lib/lib${PN}-lite.so        lib${PN}-lite-${SLOT}.so
        /usr/${host}/lib/libprotoc.so            libprotoc-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc      ${PN}-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/${PN}-lite.pc ${PN}-${SLOT}lite.pc
    )

    if option vim-syntax ; then
        insinto /usr/share/vim/vimfiles/syntax
        doins "${CMAKE_SOURCE}"/../editors/proto.vim

        alternatives+=(
            /usr/share/vim/vimfiles/syntax/proto.vim proto-${SLOT}.vim
        )
    fi

    alternatives_for _${PN} ${SLOT} ${SLOT} "${alternatives[@]}"

    edo pushd "${WORKBASE}"/${PNV}
    emagicdocs
    edo popd
}

