# Copyright 2011-2018 Timo Gurr <tgurr@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=google release=v${PV} pnv=${PN}-cpp-${PV} suffix=tar.gz ]
require alternatives

export_exlib_phases src_configure src_install src_test

SUMMARY="Protocol Buffers - Google's data interchange format"
DESCRIPTION="
Protocol buffers are a flexible, efficient, automated mechanism for serializing
structured data - think XML, but smaller, faster, and simpler. You define how
you want your data to be structured once, then you can use special generated
source code to easily write and read your structured data to and from a variety
of data streams and using a variety of languages. You can even update your data
structure without breaking deployed programs that are compiled against the old
format.
"

LICENCES="BSD-3 vim-syntax? ( vim )"
SLOT="$(ever range 1-2)"
MYOPTIONS="vim-syntax"

# TODO: options: emacs examples java
DEPENDENCIES="
    build+run:
        sys-libs/zlib[>=1.2.0.4]
    run:
        !dev-libs/protobuf:0[<3.1.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !dev-libs/protobuf:0[>3.2&<3.3.0-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    post:
        vim-syntax? ( app-editors/vim-runtime:*[>=7] )
"

WORK=${WORKBASE}/${PNV}

protobuf_src_configure() {
    local myconf=(
        --disable-static
        --with-zlib
    )
    if ! exhost --is-native -q; then
        myconf+=( --with-protoc=yes )
    fi

    CXX_FOR_BUILD=$(exhost --build)-c++ CXXCPP_FOR_BUILD=$(exhost --build)-cpp \
        econf "${myconf[@]}"
}

protobuf_src_install() {
    local alternatives=()
    local host=$(exhost --target)

    default

    alternatives+=(
        /usr/${host}/bin/protoc                  protoc-${SLOT}
        /usr/${host}/include/google/${PN}        ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.la             lib${PN}-${SLOT}.la
        /usr/${host}/lib/lib${PN}.so             lib${PN}-${SLOT}.so
        /usr/${host}/lib/lib${PN}-lite.la        lib${PN}-lite-${SLOT}.la
        /usr/${host}/lib/lib${PN}-lite.so        lib${PN}-lite-${SLOT}.so
        /usr/${host}/lib/libprotoc.la            libprotoc-${SLOT}.la
        /usr/${host}/lib/libprotoc.so            libprotoc-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc      ${PN}-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/${PN}-lite.pc ${PN}-${SLOT}lite.pc
    )

    if option vim-syntax ; then
        insinto /usr/share/vim/vimfiles/syntax
        doins "${WORK}"/editors/proto.vim

        alternatives+=(
            /usr/share/vim/vimfiles/syntax/proto.vim proto-${SLOT}.vim
        )
    fi

    alternatives_for _${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

protobuf_src_test() {
    emake check
}

